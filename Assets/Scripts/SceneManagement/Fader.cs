using System;
using System.Collections;
using UnityEngine;

namespace RPG.SceneManagement
{
    public class Fader : MonoBehaviour 
    {
        CanvasGroup canvasGroup;
        Coroutine currentActiveFade = null;
        private void Awake ()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public Coroutine FadeOut(float time)
        {   
            return Fade(1, time);
        }

        public Coroutine FadeIn(float time)
        {
            return Fade(0, time);
        }

        public Coroutine Fade(float target, float time)
        {
            // Cancel running coroutines
            if (currentActiveFade != null)
            {
                StopCoroutine(currentActiveFade);
            }
            // Run fadeout coroutine
            currentActiveFade = StartCoroutine(FadeOutRoutine(target, time));
            return currentActiveFade;
        }

        private IEnumerator FadeOutRoutine(float target, float time)
        {
            while (!Mathf.Approximately(canvasGroup.alpha, target)) // alpha is not 1
            {
                // moving alpha toward 1
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, target, Time.deltaTime / time);
                // canvasGroup.alpha += Time.deltaTime / time;
                yield return null;
            }
        }

        public void FadeOutImmediate() 
        {
            canvasGroup.alpha = 1;
        }
    }
}