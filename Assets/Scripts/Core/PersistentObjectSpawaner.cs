using UnityEngine;

namespace RPG.Core
{
    public class PersistentObjectSpawaner : MonoBehaviour 
    {
        [SerializeField] GameObject persistentObjectPrefab;
        static bool hasSpawned = false;
        private void Awake() {
            if (hasSpawned) return;
            SpawanPersistentObjects();
            hasSpawned = true;
        }

        private void SpawanPersistentObjects ()
        {
            GameObject persistentObject = Instantiate(persistentObjectPrefab);
            DontDestroyOnLoad(persistentObject);
        }
    }
}